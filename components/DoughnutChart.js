import React, {useState} from 'react';
import { Doughnut } from 'react-chartjs-2';


export default function DoughnutChart({activeCases, cases, deaths}) {

    return (

        <React.Fragment>
            <Doughnut 
            data={{
            datasets:[{
                data: [activeCases, cases, deaths],
                backgroundColor: ["blue", "red", "black"]
            }],
            labels: ["Active Cases", "Total Cases", "Deaths"]
            }}
            redraw={false}
        />
        </React.Fragment>
    )

}
