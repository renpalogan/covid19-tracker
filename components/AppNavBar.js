import {Navbar, Nav} from 'react-bootstrap'

export default function AppNavBar() {
    return (
        <Navbar bg="info"  expand="lg">
        <Navbar.Brand href="/">Covid 19 Tracker</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                <Nav.Link href="/covid/countries">Countries</Nav.Link>
                <Nav.Link href="/covid/countries/top">Top 10 Covid Cases Countries</Nav.Link>
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}