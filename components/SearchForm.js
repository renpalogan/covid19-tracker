import React from 'react'
import { Form, Button } from 'react-bootstrap'
import { useState } from 'react'



export default function SearchForm({searchCountry}) {

    const [targetCountry, setTargetCountry] = useState("")
    console.log(targetCountry)

    return(
        <React.Fragment>
            <Form onSubmit={e => searchCountry(e, targetCountry)}>
                <Form.Group controlId="formBasicCountry">
                    <Form.Label>Search Country</Form.Label>
                    <Form.Control onChange={e => setTargetCountry(e.target.value)} value={targetCountry} type="text" placeholder="Enter country name" />
                </Form.Group>
                <Button variant="primary" type="submit" block>
                    Search
                </Button>
            </Form>
        </React.Fragment>

    )
}