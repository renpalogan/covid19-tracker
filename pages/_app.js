import React from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container } from 'react-bootstrap'
import AppNavBar from '../components/AppNavBar'

function MyApp({ Component, pageProps }) {
  return (
    <React.Fragment>
    <AppNavBar/>
    <Container>
      <Component {...pageProps} />

    </Container>
    </React.Fragment>

  )
}

export default MyApp
