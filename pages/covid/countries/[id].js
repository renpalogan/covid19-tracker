import React from 'react'
import { Table } from 'react-bootstrap'
import toNum from '../../../helpers/toNum'
import DoughnutChart from '../../../components/DoughnutChart'


export default function Country({country}) {
    console.log(country)


    return ( 

        <React.Fragment>
            <h1>{country.country_name}</h1>
            <Table striped bordered hover variant="dark">
                <thead>
                <tr>
                    <th>Total Cases</th>
                    <th>Active Case</th>
                    <th>Deaths</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>{country.cases}</td>
                    <td>{country.active_cases}</td>
                    <td>{country.deaths}</td>
                </tr>
                </tbody>
            </Table>
            <br></br>
            <DoughnutChart activeCases={toNum(country.active_cases)} deaths={toNum(country.deaths)} cases={toNum(country.cases)}/>
        </React.Fragment>
        
    )
}

export async function getStaticPaths() {

    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {

        "method": "GET",
        "headers": {
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
        
        }
    });
  
    const data = await res.json();
    const paths = data.countries_stat.map(country => ({
        params: {id: country.country_name}
    }))

    return {paths, fallback: false}
    // fallback false returns an error when it cannot locate the path
}

export async function getStaticProps({ params }) {

    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {

        "method": "GET",
        "headers": {
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
        
        }
    });
  
    const data = await res.json();

    const country = data.countries_stat.find(country => country.country_name === params.id)
    return {
        props: {
            country
        }
    }

}