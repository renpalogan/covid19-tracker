import React from 'react';
import { ListGroup } from 'react-bootstrap'
import Link from 'next/link'

export default function Countries({ data }) {

    const countriesStat = data.countries_stat
    const renderCountries = countriesStat.map(country => {

        return (
                <ListGroup.Item>
                    <Link href={`/covid/countries/${country.country_name}`}>{country.country_name}</Link>
                </ListGroup.Item>
        )
    })

    return (
        <React.Fragment>
          <h1>Countries infected by Covid19 </h1>
          <ListGroup className="mt-5">
            {renderCountries}
        </ListGroup>
        </React.Fragment>
    )

}

export async function getStaticProps() {

    const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {

        "method": "GET",
        "headers": {
        "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
        "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
        
        }
    });
  
    const data = await res.json()
  
    return {
      props: {
        data
      }
    }
}