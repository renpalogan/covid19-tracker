import React, { useState } from 'react';
import Head from 'next/head'
import { Jumbotron, ListGroup } from 'react-bootstrap'
import styles from '../styles/Home.module.css'
import toNum from '../helpers/toNum'
import PageHeader from '../components/PageHeader'
import SearchForm from '../components/SearchForm'
import DoughnutChart from '../components/DoughnutChart'



export default function Home({ data }) {

  const [name, setName] = useState("")
  const [cases, setCases] = useState(0)
  const [deaths, setDeaths] = useState(0)
  const [activeCases, setActiveCases] = useState(0)

  // console.log(data.countries_stat)

  // get the sum of all covid19 cases in all the countries
  // step 1. Iterate all the countreis using map
  let total = 0;
  data.countries_stat.map(country => {
      total += toNum(country.cases)
  })
  // console.log(total) //total covid cases


  function search(e, targetCountry) {
      e.preventDefault()
      // alert (targetCountry);
      const countriesStats = data.countries_stat

      // find() will find the exact element you are looking inside an array
      const match = countriesStats.find(country => country.country_name === targetCountry)
      console.log(match)

      if (match !== undefined) {
        
        setName (match.country_name)
        setCases(toNum(match.cases))
        setDeaths(toNum(match.deaths))
        setActiveCases(toNum(match.active_cases))
        // alert('You found a country')

      }else {

        alert('Country not found')
      }
  }

    return (
      <React.Fragment>
        <PageHeader pageTitle={"Home Page"} />
        {/* <span className="pic"></span> */}
        <Jumbotron> 
          <h1>Total Covid Cases: {total}</h1>
        </Jumbotron>
        <SearchForm searchCountry={search} />
        <ListGroup className="mt-5">
          <ListGroup.Item>Country Name: {name} </ListGroup.Item>
          <ListGroup.Item>Active Cases: {activeCases} </ListGroup.Item>
          <ListGroup.Item>Total Cases: {cases} </ListGroup.Item>
          <ListGroup.Item>Deaths: {deaths} </ListGroup.Item>
        </ListGroup>
          <br></br>
        <DoughnutChart activeCases={activeCases} cases={cases} deaths={deaths} />
      </React.Fragment>
    )
}

export async function getServerSideProps() {
  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {

      "method": "GET",
      "headers": {
      "x-rapidapi-key": "6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539",
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com"
      
      }
  });

  const data = await res.json()

  return {
    props: {
      data
    }
  }

}


// getStaticProps - data does not change frequently
// getServerSideProps - data needs to be fresh, because it is changing.